package ru.bakhtiyarov.tm.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String EXIT = "exit";

    String INFO = "info";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

}
