package ru.bakhtiyarov.tm.model;

public class TerminalCommand {

    private String name = "";

    private String description = "";

    private String arg = "";

    public TerminalCommand() {
    }

    public TerminalCommand(String name, String arg) {
        this.name = name;
        this.arg = arg;
    }

    public TerminalCommand(String name, String arg, String description) {
        this.name = name;
        this.arg = arg;
        this.description = description;
    }

    public TerminalCommand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        if (name != null && !name.isEmpty()) result.append(name);
        if (arg != null && !arg.isEmpty()) result.append(", ").append(arg);
        if (description != null && !description.isEmpty()) result.append(": ").append(description);
        return result.toString();
    }

}