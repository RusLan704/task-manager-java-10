package ru.bakhtiyarov.tm.controller;

import ru.bakhtiyarov.tm.api.ICommandController;
import ru.bakhtiyarov.tm.api.ICommandService;
import ru.bakhtiyarov.tm.model.TerminalCommand;
import ru.bakhtiyarov.tm.util.NumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.9");
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Ruslan Bakhtiyarov");
        System.out.println("E-MAIL: rusya.vay@mail.ru");
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = commandService.getTerminalCommands();
        for (final TerminalCommand command : commands) System.out.println(command);
    }

    @Override
    public void displayArguments() {
        final String[] arguments = commandService.getArgs();
        System.out.println(Arrays.toString(arguments));
    }

    @Override
    public void displayCommands() {
        final String[] commands = commandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    @Override
    public void displayInfo() {
        System.out.println("INFO");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(usedMemory);
        System.out.println("Used memory by JVM: " + usedMemoryFormat);
    }

    @Override
    public void exit() {
        System.exit(0);
    }

}
