package ru.bakhtiyarov.tm.api;

import ru.bakhtiyarov.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}
