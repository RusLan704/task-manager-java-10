package ru.bakhtiyarov.tm.api;

public interface ICommandController {

    void displayVersion();

    void displayAbout();

    void displayHelp();

    void displayArguments();

    void displayCommands();

    void displayInfo();

    void displayWelcome();

    void exit();

}
