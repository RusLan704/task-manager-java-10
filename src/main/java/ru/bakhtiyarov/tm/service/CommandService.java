package ru.bakhtiyarov.tm.service;

import ru.bakhtiyarov.tm.api.ICommandRepository;
import ru.bakhtiyarov.tm.api.ICommandService;
import ru.bakhtiyarov.tm.model.TerminalCommand;

public class CommandService implements ICommandService {

    private ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    @Override
    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public String[] getArgs() {
        return commandRepository.getArgs();
    }

}
